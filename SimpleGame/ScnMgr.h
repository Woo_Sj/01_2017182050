#pragma once

#include "Globals.h"
#include "Renderer.h"
#include "Object.h"
#include "Physics.h"
#include "Sound.h"
#include "RoundPattern.h"

class ScnMgr
{
public:
	ScnMgr();
	~ScnMgr();

	void RenderScene();

	int AddObject(
		float x, float y, float z,
		float sx, float sy, float sz,
		float r, float g, float b, float a,
		float vx, float vy, float vz,
		float mass,
		float fricCoef,
		float hp,
		int type);

	void Update(float eTime);
	void KeyDownInput(unsigned char key, int x, int y);
	void KeyUpInput(unsigned char key, int x, int y);
	void SpecialKeyDownInput(unsigned char key, int x, int y);
	void SpecialKeyUpInput(unsigned char key, int x, int y);

	void InitObstacle();

private:
	void DeleteObject(int idx);
	void DoGarbageColletion();

private:
	Renderer* m_Renderer = nullptr;
	Physics* m_Physics = nullptr;
	Sound* m_Sound = nullptr;

	Object* m_Objects[MAX_OBJECTS] = { nullptr, };
	RoundPattern* m_FirstRoundPattern = nullptr;

	// Key inputs
	bool m_keyW = false;
	bool m_keyA = false;
	bool m_keyS = false;
	bool m_keyD = false;
	bool m_KeySP = false;

	// special key inputs
	bool m_keyUp = false;
	bool m_keyLeft = false;
	bool m_keyDown = false;
	bool m_keyRight = false;
};

