#pragma once

#define MAX_OBJECTS 1000

#define HERO_ID 0

#define GRAVITY 9.8f

#define TYPE_HERO -1
#define TYPE_NORMAL 0
#define TYPE_BULLET 1
#define TYPE_OBSTACLE 2
#define TYPE_GAMEOVER 10

#define BACKGROUND_X 1024
#define BACKGROUND_Y 722

#define WINDOW_X 1024
#define WINDOW_Y 722