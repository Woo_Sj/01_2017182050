#pragma once
class Object
{
public:
	Object();
	~Object();

	void InitPhysics();

	void GetPos(float* x, float* y, float* z);
	void SetPos(float x, float y, float z);

	void GetVel(float* x, float* y, float* z);
	void SetVel(float x, float y, float z);

	void GetAcc(float* x, float* y, float* z);
	void SetAcc(float x, float y, float z);

	void GetVol(float* x, float* y, float* z);
	void SetVol(float x, float y, float z);

	void GetColor(float* r, float* g, float* b, float* a);
	void SetColor(float r, float g, float b, float a);

	void GetMass(float* mass);
	void SetMass(float mass);

	void GetFrictCoef(float* coef);
	void SetFrictCoef(float coef);

	void GetType(int* type);
	void SetType(int type);

	void GetTex(int* id);
	void SetTex(int id);

	float GetAge();
	
	inline bool IsActive() const { return m_bActive; }
	void SetActive(const bool bActive) { m_bActive = bActive; }

	void SetParentObj(Object* obj);
	Object* GetParentObj();
	bool IsAncestor(Object* obj);

	void Update(float eTime);

	void AddForce(float x, float y, float z, float eTime);

	bool CanShootBullet();
	void ResetShootBulletCoolTime();

	void SetHP(float hp);
	void GetHP(float* hp);
	void Damage(float damage);

	inline void GetCollide(bool* col) { *col = m_isCollide; };
	inline void SetCollide(bool col) { m_isCollide = col; };

private:
	float m_posX, m_posY, m_posZ;
	float m_velX, m_velY, m_velZ;
	float m_accX, m_accY, m_accZ;
	float m_volX, m_volY, m_volZ;
	float m_r, m_g, m_b, m_a;
	float m_mass;
	float m_frictCoef;
	float m_healthPoint;
	float m_age;
	int m_type;

	bool m_isCollide = false;

	Object* m_parent = NULL;

	float m_remainingBulletCoolTime = 0.f;
	float m_defaultBulletCoolTime = 0.2f;

	int m_texID = -1;

	bool m_bActive = true;
};

