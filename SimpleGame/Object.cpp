#include "stdafx.h"
#include "Object.h"
#include "Globals.h"
#include <math.h>


Object::Object()
{
	InitPhysics();

	m_remainingBulletCoolTime =
		m_defaultBulletCoolTime;

	m_age = 0.f;
}


Object::~Object()
{
}

void Object::InitPhysics()
{
	m_posX = 0.f, m_posY = 0.f, m_posZ = 0.f;
	m_velX = 0.f, m_velY = 0.f, m_velZ = 0.f;
	m_accX = 0.f, m_accY = 0.f, m_accZ = 0.f;
	m_volX = 0.f, m_volY = 0.f, m_volZ = 0.f;
	//m_r = 0.f, m_g = 0.f, m_b = 0.f, m_a = -1.f;
	m_mass = -1.f;
	m_frictCoef = 0.f;
}

float Object::GetAge()
{
	return m_age;
}

void Object::GetPos(float* x, float* y, float* z)
{
	*x = m_posX;
	*y = m_posY;
	*z = m_posZ;
}

void Object::SetPos(float x, float y, float z)
{
	m_posX = x;
	m_posY = y;
	m_posZ = z;
}

void Object::GetVel(float* x, float* y, float* z)
{
	*x = m_velX;
	*y = m_velY;
	*z = m_velZ;
}

void Object::SetVel(float x, float y, float z)
{
	m_velX = x;
	m_velY = y;
	m_velZ = z;
}

void Object::GetAcc(float* x, float* y, float* z)
{
	*x = m_accX;
	*y = m_accY;
	*z = m_accZ;
}

void Object::SetAcc(float x, float y, float z)
{
	m_accX = x;
	m_accY = y;
	m_accZ = z;
}

void Object::GetVol(float* x, float* y, float* z)
{
	*x = m_volX;
	*y = m_volY;
	*z = m_volZ;
}

void Object::SetVol(float x, float y, float z)
{
	m_volX = x;
	m_volY = y;
	m_volZ = z;
}

void Object::GetColor(float* r, float* g, float* b, float* a)
{
	*r = m_r;
	*g = m_g;
	*b = m_b;
	*a = m_a;
}

void Object::SetColor(float r, float g, float b, float a)
{
	m_r = r;
	m_g = g;
	m_b = b;
	m_a = a;
}

void Object::GetMass(float* mass)
{
	*mass = m_mass;
}

void Object::SetMass(float mass)
{
	m_mass = mass;
}

void Object::GetFrictCoef(float* coef)
{
	*coef = m_frictCoef;
}
void Object::SetFrictCoef(float coef)
{
	m_frictCoef = coef;
}

void Object::GetType(int* type)
{
	*type = m_type;
}

void Object::SetType(int type)
{
	m_type = type;
}

void Object::GetTex(int* id)
{
	*id = m_texID;
}

void Object::SetParentObj(Object* obj)
{
	m_parent = obj;
}

Object* Object::GetParentObj()
{
	return m_parent;
}

bool Object::IsAncestor(Object* obj)
{
	//TBD
	if (m_parent != NULL)
	{
		if (obj == m_parent)
		{
			return true;
		}
	}
	return false;
}

void Object::SetTex(int id)
{
	m_texID = id;
}

bool Object::CanShootBullet()
{
	if (m_remainingBulletCoolTime < 0.000000001f)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Object::ResetShootBulletCoolTime()
{
	m_remainingBulletCoolTime = m_defaultBulletCoolTime;
}

void Object::Update(float eTime)
{
	//Reduce bullet cooltime
	m_remainingBulletCoolTime -= eTime;

	//add to age
	m_age += eTime;

	///////////////Apply Friction/////////////////
	//normalize velocity vector(only x,y)
	float velSize = sqrtf(m_velX * m_velX + m_velY * m_velY);
	if (velSize > 0.f)
	{
		float vX = m_velX / velSize;
		float vY = m_velY / velSize;

		//calculate friction size
		float nForce = GRAVITY * m_mass;
		float frictionSize = m_frictCoef * nForce;
		float fX = -vX * frictionSize;
		float fY = -vY * frictionSize;

		//calculate acc from friction
		float accX = fX / m_mass;
		float accY = fY / m_mass;

		//Update velocity
		float newVelX = m_velX + accX * eTime;
		float newVelY = m_velY + accY * eTime;
		m_velZ = m_velZ - GRAVITY * eTime;
		if (newVelX * m_velX < 0.f)
		{
			m_velX = 0.f;
		}
		else
		{
			m_velX = newVelX;
		}
		if (newVelY * m_velY < 0.f)
		{
			m_velY = 0.f;
		}
		else
		{
			m_velY = newVelY;
		}
	}
	else if (m_posZ > 0.f)
	{
		m_velZ = m_velZ - GRAVITY * eTime;
	}
	//////////////////////////////////////////////

	m_posX = m_posX + m_velX * eTime;
	m_posY = m_posY + m_velY * eTime;
	m_posZ = m_posZ + m_velZ * eTime;

	if (m_posZ < 0.f)
	{
		m_posZ = 0.f;
		m_velZ = 0.f;
	}
}

void Object::AddForce(float x, float y, float z, float eTime)
{
	float accX, accY, accZ;
	accX = accY = accZ = 0.f;

	accX = x / m_mass;
	accY = y / m_mass;
	accZ = z / m_mass;

	m_velX = m_velX + accX * eTime;
	m_velY = m_velY + accY * eTime;
	m_velZ = m_velZ + accZ * eTime;
}

void Object::SetHP(float hp)
{
	m_healthPoint = hp;
}

void Object::GetHP(float* hp)
{
	*hp = m_healthPoint;
}

void Object::Damage(float damage)
{
	m_healthPoint -= damage;
}