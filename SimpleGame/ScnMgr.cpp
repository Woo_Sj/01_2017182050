#include "stdafx.h"

#include "ScnMgr.h"
#include "Dependencies\freeglut.h"

int g_BackGround = -1;
int g_ObstacleTex = -1;

int g_BulletTex = -1;

int g_testParticle = -1;
int g_testParticleTex = -1;

int g_SoundBGM = -1;
int g_Sound_Bullet_fire = -1;
int g_SoundExplosion = -1;

int g_gameover = -1;

float gameoverhp = 0;
default_random_engine dre;
uniform_int_distribution<> uid(1, 3);

ScnMgr::ScnMgr()
{
	m_Renderer = new Renderer(WINDOW_X, WINDOW_Y);
	if (!m_Renderer->IsInitialized())
	{
		assert(false && "Renderer could not be initialized..");
	}

	m_Physics = new Physics();
	m_Sound = new Sound();

	// Initialize hero
	{
		// 오브젝트 배열에서 히어로 오브젝트 인덱스는 항상 0(HERO_ID)으로 간주한다.
		AddObject(0.0f, 0.0f, 0.0f,
			0.5f, 0.5f, 0.5f,
			1.0f, 1.0f, 1.0f, 1.0f,
			0.0f, 0.0f, 0.0f,
			10.0f,
			0.2f,
			100.0f,
			TYPE_HERO);

		const int heroAnimTex = m_Renderer->GenPngTexture("./Textures/hero.png");
		m_Objects[HERO_ID]->SetTex(heroAnimTex);
	}

	g_BackGround = m_Renderer->GenPngTexture("./Textures/battlebackground.png");
	g_testParticleTex = m_Renderer->GenPngTexture("./Textures/particle.png");
	g_testParticle = m_Renderer->CreateParticleObject(
		50,
		-0, -0,
		0, 0,
		5, 5,
		7, 7,
		-10, -10,
		10, 10);

	g_BulletTex = m_Renderer->GenPngTexture("./Textures/bullet.png");
	g_gameover = m_Renderer->GenPngTexture("./Textures/gameover.png");
	int gameover = AddObject(0, 0, 0, 100, 10, 10, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, TYPE_GAMEOVER);
	m_Objects[gameover]->SetActive(false);
	// Initialize monster
	/*{
		const int monsterTex = m_Renderer->GenPngTexture("./Textures/hero.png");

		int temp = AddObject(
			2.f, 0.f, 0.f,
			0.5f, 0.5f, 0.5f,
			1.f, 1.f, 1.f, 1.f,
			0.f, 0.f, 0.f,
			20.f,
			0.7f,
			10.f,
			TYPE_NORMAL);
		m_Objects[temp]->SetTex(monsterTex);

		temp = AddObject(
			-2.f, 0.f, 0.f,
			0.5f, 0.5f, 0.5f,
			1.f, 1.f, 1.f, 1.f,
			0.f, 0.f, 0.f,
			20.f,
			0.7f,
			10.f,
			TYPE_NORMAL);
		m_Objects[temp]->SetTex(monsterTex);

		temp = AddObject(
			-2.f, -2.f, 0.f,
			0.5f, 0.5f, 0.5f,
			1.f, 1.f, 1.f, 1.f,
			0.f, 0.f, 0.f,
			20.f,
			0.7f,
			10.f,
			TYPE_NORMAL);
		m_Objects[temp]->SetTex(monsterTex);
	}*/

	//BGM
	g_SoundBGM = m_Sound->CreateBGSound("./Sounds/battlebgm.wav");
	m_Sound->PlayBGSound(g_SoundBGM, true, 1.f);

	g_Sound_Bullet_fire = m_Sound->CreateShortSound("./Sounds/snd_arrow.wav");
	g_SoundExplosion = m_Sound->CreateShortSound("./Sounds/snd_damage.wav");

	// Initialize first round pattern
	InitObstacle();
}

ScnMgr::~ScnMgr()
{
	if (m_FirstRoundPattern != nullptr)
	{
		delete m_FirstRoundPattern;
		m_FirstRoundPattern = nullptr;
	}

	for (int i = 0; i < MAX_OBJECTS; ++i)
	{
		if (m_Objects[i] != nullptr)
		{
			delete m_Objects[i];
			m_Objects[i] = nullptr;
		}
	}

	if (m_Sound != nullptr)
	{
		delete m_Sound;
		m_Sound = nullptr;
	}

	if (m_Physics != nullptr)
	{
		delete m_Physics;
		m_Physics = nullptr;
	}

	if (m_Renderer != nullptr)
	{
		delete m_Renderer;
		m_Renderer = nullptr;
	}
}

void ScnMgr::InitObstacle(){
	constexpr int obstacleCount = 500;
	const int obstacleTex = m_Renderer->GenPngTexture("./Textures/Obstacle.png");
	const int obstacleRightTex = m_Renderer->GenPngTexture("./Textures/Obstacle_right.png");
	const int obstacleLeftTex = m_Renderer->GenPngTexture("./Textures/Obstacle_left.png");


	m_FirstRoundPattern = new RoundPattern(obstacleCount);

	for (int i = 0; i < obstacleCount; ++i)
	{
		if (uid(dre) == 1) {
			int obstacleID = AddObject(
				1.f, 2.f, 0.f,
				0.2f, 1.0f, 1.f,
				1.f, 1.f, 1.f, 1.f,
				0.f, -10.f, 0.f,
				1.f,
				0.7f,
				10.f,
				TYPE_OBSTACLE);

			m_Objects[obstacleID]->SetActive(false);
			m_Objects[obstacleID]->SetTex(obstacleTex);
			m_FirstRoundPattern->AddObstacle(m_Objects[obstacleID]);
		}
		else if (uid(dre) == 2) {
			int obstacleID = AddObject(
				-10.f, 0.f, 0.f,
				1.0f, 0.2f, 1.f,
				1.f, 1.f, 1.f, 1.f,
				10.f, 0.f, 0.f,
				1.f,
				0.7f,
				11.f,
				TYPE_OBSTACLE);

			m_Objects[obstacleID]->SetActive(false);
			m_Objects[obstacleID]->SetTex(obstacleRightTex);
			m_FirstRoundPattern->AddObstacle(m_Objects[obstacleID]);
		}
		else if (uid(dre) == 2) {
			int obstacleID = AddObject(
				10.f, 0.f, 0.f,
				1.0f, 0.2f, 1.f,
				1.f, 1.f, 1.f, 1.f,
				-10.f, 0.f, 0.f,
				1.f,
				0.7f,
				11.f,
				TYPE_OBSTACLE);

			m_Objects[obstacleID]->SetActive(false);
			m_Objects[obstacleID]->SetTex(obstacleLeftTex);
			m_FirstRoundPattern->AddObstacle(m_Objects[obstacleID]);
		}
	}
}

void ScnMgr::Update(float eTime)
{
	
	// Update Collision
	for (int src = 0; src < MAX_OBJECTS; src++)
	{
		for (int trg = src + 1; trg < MAX_OBJECTS; trg++)
		{
			if (m_Objects[src] != nullptr && m_Objects[src]->IsActive()
				&& m_Objects[trg] != nullptr && m_Objects[trg]->IsActive())
			{
				if (m_Physics->IsOverlap(m_Objects[src], m_Objects[trg]))
				{
					if (!m_Objects[src]->IsAncestor(m_Objects[trg]) && !m_Objects[trg]->IsAncestor(m_Objects[src]))
					{
						int srcType;
						int trgType;
						float x, y, z;
						float vx, vy, vz;
						m_Objects[src]->GetType(&srcType);
						m_Objects[trg]->GetType(&trgType);
						m_Objects[src]->GetPos(&x, &y, &z);
						m_Objects[src]->GetVel(&vx, &vy, &vz);

						if (srcType != trgType) 
						{
							m_Physics->ProcessCollision(m_Objects[src], m_Objects[trg]);

							float srcHP, trgHP;
							m_Objects[src]->GetHP(&srcHP);
							m_Objects[trg]->GetHP(&trgHP);
							m_Objects[src]->SetHP(srcHP - trgHP);
							m_Objects[trg]->SetHP(trgHP - srcHP);

							//collide check
							if(src==HERO_ID ||trg==HERO_ID)
								m_Objects[HERO_ID]->SetCollide(true);
							
							m_Sound->PlayShortSound(g_SoundExplosion, false, 0.6f);
						}
					}
				}
			}
		}
	}

	int type;
	m_Objects[HERO_ID]->GetType(&type);

	// Update Hero
	if (m_Objects[HERO_ID] != nullptr && type == TYPE_HERO)
	{
		float heroX, heroY, heroZ;
		float heroMoveSpeed = 3.f;

		float fx, fy, fz;
		float fAmount = 100.f;
		fx = fy = fz = 0.f;

		float clampX = 4.1f;
		float clampY = -0.3f;
		float clamp_y = -2.2f;
		m_Objects[HERO_ID]->GetPos(&heroX, &heroY, &heroZ);

		//white box clamp = -x = -4.1, x = 4.1, +y = 0.1, -y = 2.2

		if (m_keyA && -clampX < heroX)
			heroX -= heroMoveSpeed * eTime;
		if (m_keyD && heroX < clampX)
			heroX += heroMoveSpeed * eTime;
		if (m_keyS && clamp_y < heroY)
			heroY -= heroMoveSpeed * eTime;
		if (m_keyW && heroY < clampY)
			heroY += heroMoveSpeed * eTime;

		m_Objects[HERO_ID]->SetPos(heroX, heroY, heroZ);
	}

	// Update BulletShoot
	{
		float fxBullet{ 0.f }, fyBullet{ 0.f }, fzBullet{ 0.f };
		float vAmountBullet{ 5.f };
		float bulletSpeed{ 5.f };

		if (m_keyUp)fyBullet += 1.f;
		if (m_keyDown)fyBullet -= 1.f;
		if (m_keyLeft)fxBullet -= 1.f;
		if (m_keyRight)fxBullet += 1.f;

		float fbSize = sqrtf(fxBullet * fxBullet +
			fyBullet * fyBullet +
			fzBullet * fzBullet);

		if (fbSize > FLT_EPSILON)
		{
			fxBullet /= fbSize;
			fyBullet /= fbSize;
			fzBullet /= fbSize;

			fxBullet *= vAmountBullet;
			fyBullet *= vAmountBullet;
			fzBullet *= vAmountBullet;

			// Add object for firing bullet
			float hX, hY, hZ;

			m_Objects[HERO_ID]->GetVel(&hX, &hY, &hZ);

			hX = hX + fxBullet * bulletSpeed;
			hY = hY + fyBullet * bulletSpeed;
			hZ = 0.f;

			float x, y, z;
			m_Objects[HERO_ID]->GetPos(&x, &y, &z);

			float s = 0.25f;
			float mass = 1.f;
			float fricCoef = 0.8f;
			int type = TYPE_BULLET;

			// Check whether this obj can shoot bullet or not
			if (m_Objects[HERO_ID]->CanShootBullet())
			{
				int temp = AddObject(x, y, z,
					s, s, s,
					1, 1, 1, 1,
					hX, hY, hZ,
					mass,
					fricCoef,
					10,
					type);

				m_Objects[temp]->SetParentObj(m_Objects[HERO_ID]);
				m_Objects[HERO_ID]->ResetShootBulletCoolTime();
				m_Sound->PlayShortSound(g_Sound_Bullet_fire, false, 0.6f);
			}
		}
	}

	m_FirstRoundPattern->UpdateObstacle(eTime);

	// Update object
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		if (m_Objects[i] != nullptr && m_Objects[i]->IsActive())
		{
			m_Objects[i]->Update(eTime);

		}
	}
	float hp;
	m_Objects[HERO_ID]->GetHP(&hp);
	gameoverhp = hp;

	if (hp < FLT_EPSILON) {
		static float time;
		time += 0.016f;

		for (int i = 1; i < MAX_OBJECTS; i++) {
			m_Objects[i] = nullptr;
		}
		m_Sound->StopBGSound(g_SoundBGM);
		if (time > 3.f)
			exit(0);
	}
}

void ScnMgr::RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.3f, 0.3f, 1.0f);

	//BackGround
	m_Renderer->DrawGround(0, 0, 0, BACKGROUND_X, BACKGROUND_Y, 0, 1, 1, 1, 1, g_BackGround, 1.f);

	if (gameoverhp < 0.f) {
		m_Renderer->DrawGround(0, 0, 0, BACKGROUND_X, BACKGROUND_Y, 1, 1, 1, 1, 1, g_gameover, 1.f);
	}
	static float colltime = 0.f;
	static float pTime = 0.f;
	float aTime = 0.016f;
	pTime += aTime;
	

	// Draw texture and gauge
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		if (m_Objects[i] == nullptr
			|| m_Objects[i]->IsActive() == false)
		{
			continue;
		}

		float x, y, z = 0.f;
		float sx, sy, sz = 0.f;
		float r, g, b, a = 0.f;
		int texID = -1;
		
		m_Objects[i]->GetPos(&x, &y, &z);
		x = x * 100.f;
		y = y * 100.f;
		z = z * 100.f;
		m_Objects[i]->GetVol(&sx, &sy, &sz);
		sx = sx * 100.f;
		sy = sy * 100.f;
		sz = sz * 100.f;
		m_Objects[i]->GetColor(&r, &g, &b, &a);
		m_Objects[i]->GetTex(&texID);

		if (i == HERO_ID)
		{
			m_Renderer->DrawTextureRect(
				x, y, z,
				sx, sy, sz,
				r, g, b, a,
				texID);

			int type = 0;
			m_Objects[i]->GetType(&type);

			float hp = 0;
			m_Objects[i]->GetHP(&hp);

			// Draw hp gage
			if (hp >= 0.0f)
			{
				m_Renderer->DrawSolidRectGauge(
					x, y, z,
					0, sy / 2.f + 5, 0,
					hp, 10, 1,
					0, 1, 0, 1,
					100, false);
			}
		}
		else
		{
			int type = 0;
			m_Objects[i]->GetType(&type);

			if (type == TYPE_BULLET) {
				m_Renderer->DrawTextureRect(
					x, y, z,
					sx, sy, sz,
					r, g, b, a, g_BulletTex
				);
			}
			else {
				m_Renderer->DrawTextureRect(
					x, y, z,
					sx, sy, sz,
					r, g, b, a, texID
				);
			}
		}

		float vx, vy, vz;
		m_Objects[i]->GetVel(&vx, &vy, &vz);

		//Draw Particle
		bool col;
		m_Objects[i]->GetCollide(&col);

		if (col == true) {
			colltime += aTime;
			m_Renderer->DrawParticle(
				g_testParticle,
				x, y + 0.05f , z,
				1,
				1, 1, 1, 1,
				-vx * 10, -vy * 10,
				g_testParticleTex,
				100.f,
				m_Objects[i]->GetAge() * 10);

			if (colltime > 0.3f) {

				m_Objects[i]->SetCollide(false);
				colltime = 0.f;
			}
		}
	}

	DoGarbageColletion();
}

int ScnMgr::AddObject(
	float x, float y, float z,
	float sx, float sy, float sz,
	float r, float g, float b, float a,
	float vx, float vy, float vz,
	float mass,
	float fricCoef,
	float hp,
	int type)
{
	// Search empty slot
	int idx = -1;
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		if (m_Objects[i] == nullptr)
		{
			idx = i;
			break;
		}
	}

	assert(idx >= 0 && "No more remaining object");

	m_Objects[idx] = new Object();
	m_Objects[idx]->SetColor(r, g, b, a);
	m_Objects[idx]->SetPos(x, y, z);
	m_Objects[idx]->SetVol(sx, sy, sz);
	m_Objects[idx]->SetVel(vx, vy, vz);
	m_Objects[idx]->SetMass(mass);
	m_Objects[idx]->SetFrictCoef(fricCoef);
	m_Objects[idx]->SetType(type);
	m_Objects[idx]->SetHP(hp);

	return idx;
}

void ScnMgr::DeleteObject(int idx)
{
	if (idx < 0)
	{
		std::cout << "Negative idx does not allowed. \n";
		return;
	}

	assert(idx >= 0 && "No more remaining object");

	if (m_Objects[idx] != nullptr)
	{
		delete m_Objects[idx];
		m_Objects[idx] = nullptr;
	}
}

void ScnMgr::KeyDownInput(unsigned char key, int x, int y)
{
	// TODO: if문 안 쓰고 대입하는 방법도 좋을 것 같습니다. ex) m_keyW = (key == 'w' || key == 'W');
	if (key == 'w' || key == 'W')
	{
		m_keyW = true;
	}
	if (key == 'a' || key == 'A')
	{
		m_keyA = true;
	}
	if (key == 's' || key == 'S')
	{
		m_keyS = true;
	}
	if (key == 'd' || key == 'D')
	{
		m_keyD = true;
	}
	if (key == ' ')
	{
		m_KeySP = true;
	}
}

void ScnMgr::KeyUpInput(unsigned char key, int x, int y)
{
	// TODO: if문 안 쓰고 대입하는 방법도 좋을 것 같습니다. ex) m_keyW = !(key == 'w' || key == 'W');
	if (key == 'w' || key == 'W')
	{
		m_keyW = false;
	}
	if (key == 'a' || key == 'A')
	{
		m_keyA = false;
	}
	if (key == 's' || key == 'S')
	{
		m_keyS = false;
	}
	if (key == 'd' || key == 'D')
	{
		m_keyD = false;
	}
	if (key == ' ')
	{
		m_KeySP = false;
	}
}

void ScnMgr::SpecialKeyDownInput(unsigned char key, int x, int y)
{
	// TODO: if문 안 쓰고 대입하는 방법도 좋을 것 같습니다. ex) m_keyUp = (key == GLUT_KEY_UP);
	if (key == GLUT_KEY_UP)
	{
		m_keyUp = true;
	}
	if (key == GLUT_KEY_LEFT)
	{
		m_keyLeft = true;
	}
	if (key == GLUT_KEY_RIGHT)
	{
		m_keyRight = true;
	}
	if (key == GLUT_KEY_DOWN)
	{
		m_keyDown = true;
	}
}

void ScnMgr::SpecialKeyUpInput(unsigned char key, int x, int y)
{
	// TODO: if문 안 쓰고 대입하는 방법도 좋을 것 같습니다. ex) m_keyUp = !(key == GLUT_KEY_UP);
	if (key == GLUT_KEY_UP)
	{
		m_keyUp = false;
	}
	if (key == GLUT_KEY_LEFT)
	{
		m_keyLeft = false;
	}
	if (key == GLUT_KEY_RIGHT)
	{
		m_keyRight = false;
	}
	if (key == GLUT_KEY_DOWN)
	{
		m_keyDown = false;
	}
}

void ScnMgr::DoGarbageColletion()
{
	// delete bullets those zero vel
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		if (m_Objects[i] == nullptr)
		{
			continue;
		}

		int type;
		m_Objects[i]->GetType(&type);

		if (type == TYPE_BULLET || type == TYPE_OBSTACLE)
		{
			float vx, vy, vz;
			m_Objects[i]->GetVel(&vx, &vy, &vz);
			float vSize = sqrtf(vx * vx + vy * vy + vz * vz);

			if (vSize < FLT_EPSILON)
			{
				DeleteObject(i);
				continue;
			}
		}

		float hp;
		m_Objects[i]->GetHP(&hp);

		if (hp < FLT_EPSILON)
		{
			m_Objects[i]->SetActive(false);
			continue;
		}
	}
}