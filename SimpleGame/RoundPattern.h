#pragma once

#include "stdafx.h"
#include <vector>

#include "Object.h"

class RoundPattern final
{
public:
	RoundPattern(const int obstacleCount);

	~RoundPattern();

	RoundPattern(const RoundPattern&) = delete;

	RoundPattern& operator=(const RoundPattern&) = delete;

	inline bool IsActive() const { return m_bActive; }

	void SetActive(const bool bActive) { m_bActive = bActive; }

	void AddObstacle(Object* obstacle);

	void UpdateObstacle(const float eTime);

private:
	const int MAX_OBSTACLE_COUNT = 0;

	int m_ObstacleCount = 0;

	int m_UsedObstacleCount = 0;

	float m_RemainingDealy = 0.0f;

	bool m_bActive = true;

	// Object 자체는 ScnMgr에서 해제되므로 이 곳에서는 해제할 필요가 없다.
	Object** m_Obstacles = nullptr;
};

