#include "stdafx.h"
#include <cassert>
#include <random>

#include "RoundPattern.h"
#include "Object.h"
#include "Global.h"

using namespace std;

//white box clamp = -x = -4.1, x = 4.1, +y = -0.3, -y = -2.2

static default_random_engine g_Dre;
static uniform_int_distribution<> g_UidPos_X(-4.1f, 4.1f);
static uniform_int_distribution<> g_UidPos_Y(-2.2f, -0.3f);

RoundPattern::RoundPattern(const int obstacleCount)
	: MAX_OBSTACLE_COUNT(obstacleCount)
	, m_RemainingDealy(5.0f)
{
	m_Obstacles = new Object * [MAX_OBSTACLE_COUNT];
}

RoundPattern::~RoundPattern()
{
	if (m_Obstacles != nullptr)
	{
		delete m_Obstacles;
		m_Obstacles = nullptr;
	}
}

void RoundPattern::AddObstacle(Object* obstacle)
{
	assert(obstacle != nullptr && "the obstacle must not be null");
	assert(m_ObstacleCount + 1 <= MAX_OBSTACLE_COUNT && "최대 담을 수 있는 개수를 넘겼습니다.");

	m_Obstacles[m_ObstacleCount++] = obstacle;
}

void RoundPattern::UpdateObstacle(const float eTime)
{
	if (m_bActive == false)
	{
		return;
	}

	
	constexpr float delay = 0.1f;
	float hp;
	m_RemainingDealy -= eTime;

	if (m_RemainingDealy <= 0.0f)
	{
		// 새로운 장애물을 활성화한다.
		m_Obstacles[m_UsedObstacleCount]->GetHP(&hp);

		if (hp == 10.f) {
			m_Obstacles[m_UsedObstacleCount]->SetPos(g_UidPos_X(g_Dre), 4.f, 0.0f);
			m_Obstacles[m_UsedObstacleCount]->SetActive(true);
		}
		else {
			m_Obstacles[m_UsedObstacleCount]->SetPos(g_UidPos_X(g_Dre), g_UidPos_Y(g_Dre), 0.0f);
			m_Obstacles[m_UsedObstacleCount]->SetActive(true);
		}

		if (++m_UsedObstacleCount >= MAX_OBSTACLE_COUNT)
		{
			m_bActive = false;
			return;
		}

		m_RemainingDealy = delay;
	}
}
